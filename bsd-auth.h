
#ifndef _BSD_AUTH_H
#define _BSD_AUTH_H 1

#include "attributes.h"
#include "compat.h"

extern void authuser(char const *restrict doas_prompt,
		     char const *restrict name,
		     char const *restrict login_style,
		     bool persist) __nonnull((1, 2));

#endif /* _BSD_AUTH_H */
