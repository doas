
BEGIN { format = (HAVE_GPERF == 1) ? "%s, %u, %s\n" : "{ \"%s\", %u, %s },\n"; }

{
	printf format, $1, length($1), $2
}
