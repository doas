
#ifndef _PAM_H
#define _PAM_H 1

#include "compat.h"

extern void pamauth(char const *restrict doas_prompt, char const *restrict target_name, char const *restrict original_name);

#endif
