
#ifndef _SHADOW_H
#define _SHADOW_H 1

#include "attribute.h"
#include "compat.h"

extern void shadowauth(char const *restrict doas_prompt, char const *name) __nonnull((1));

#endif /* _SHADOW_H */
